using System;
using System.IO;
using System.Net.WebSockets;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Google.Protobuf;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CqgMockService
{
    public class Startup
    {
        RandomGen randomGen = new RandomGen();
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var webSocketOptions = new WebSocketOptions()
            {
                KeepAliveInterval = TimeSpan.FromSeconds(120),
                ReceiveBufferSize = 4 * 1024
            };

            app.UseWebSockets(webSocketOptions);
            //if (env.IsDevelopment())
            //{
            //  app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            //  app.UseExceptionHandler("/Home/Error");
            //  // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            //  app.UseHsts();
            //}
            //app.UseHttpsRedirection();
            //app.UseStaticFiles();

            //app.UseRouting();

            //app.UseAuthorization();

            app.Use(async (context, next) =>
            {
                if (context.Request.Path == "/ws")
                {
                    if (context.WebSockets.IsWebSocketRequest)
                    {
                        System.Net.WebSockets.WebSocket webSocket = await context.WebSockets.AcceptWebSocketAsync();
                        await WebSocketWaitLoop(context, webSocket);
                    }
                    else
                    {
                        context.Response.StatusCode = 400;
                    }
                }
                else
                {
                    await next();
                }

            });

            //app.UseEndpoints(endpoints =>
            //{
            //  endpoints.MapControllerRoute(
            //            name: "default",
            //            pattern: "{controller=Home}/{action=Index}/{id?}");
            //});
        }

        private async Task WebSocketWaitLoop(HttpContext context, System.Net.WebSockets.WebSocket webSocket)
        {

            while (webSocket.State.HasFlag(WebSocketState.Open))
            {
                try
                {
                    var buffer = new ArraySegment<byte>(new byte[2048]);
                    WebSocketReceiveResult result;
                    using var ms = new MemoryStream();
                    do
                    {
                        result = await webSocket.ReceiveAsync(buffer, CancellationToken.None);
                        ms.Write(buffer.Array, buffer.Offset, result.Count);

                    } while (!result.EndOfMessage);

                    ms.Position = 0;
                    ClientMsg msg = ClientMsg.Parser.ParseFrom(ms);

                    if (msg.Logon != null)
                        await ProcessHasLogon(msg.Logon, webSocket);
                    else if (msg.MetaData != null)
                        await ProcessMetaDataRequest(msg.MetaData, webSocket);
                    else if (msg.TraderMetrics != null)
                        await ProcessTraderMetricsRequest(msg.TraderMetrics, webSocket);
                    else if (msg.CqgStatus != null)
                        await ProcessCqgStatusRequest(msg.CqgStatus, webSocket);
                    else if (msg.Ping != null)
                        await ProcessPingRequest(msg.Ping, webSocket);
                    else if (msg.Pong != null)
                        await ProcessPongRequest(msg.Pong, webSocket);
                    else if (msg.EnableCqgAccount != null)
                        await ProcessEnableCQGAccountRequest(msg.EnableCqgAccount, webSocket);
                    else if (msg.NewMt5 != null)
                        await ProcessNewMt5Request(msg.NewMt5, webSocket);
                    else if (msg.ResetUserMt5 != null)
                        await ProcessResetUserMt5Request(msg.ResetUserMt5, webSocket);
                    else if (msg.ResetPassMt5 != null)
                        await ProcessResetPassMt5Request(msg.ResetPassMt5, webSocket);
                    else if (msg.ChangeBalanceMt5 != null)
                        await ProcessChangeBalanceMt5Request(msg.ChangeBalanceMt5, webSocket);
                    else if (msg.DisableUserMt5 != null)
                        await ProcessDisableUserMt5Request(msg.DisableUserMt5, webSocket);
                    else if (msg.VerifyEmail != null)
                        await ProcessVerifyEmailRequest(msg.VerifyEmail, webSocket);
                    else if (msg.NewUser != null)
                        await ProcessNewUserRequest(msg.NewUser, webSocket);
                    else if (msg.ActivateTradingPlatform != null)
                        await ProcessTradingPlatform(msg.ActivateTradingPlatform, webSocket);
                    else if (msg.ActivateLiveMarketDataNoDocs != null)
                        await ProcessLiveMarketDataNoDocsRequest(msg.ActivateLiveMarketDataNoDocs, webSocket);
                    else if (msg.ResetPassword != null)
                        await ProcessResetPasswordRequest(msg.ResetPassword, webSocket);
                    else if (msg.NewTradingAccount != null)
                        await ProcessNewTradingAccountResetRequest(msg.NewTradingAccount, webSocket);
                    else if (msg.ActivateNewMarketData != null)
                        await ProcessActivateNewMarketDataRequest(msg.ActivateNewMarketData, webSocket);
                    else if (msg.DisableAccount != null)
                        await ProcessDisableAccountRequest(msg.DisableAccount, webSocket);
                    else if (msg.DisableMarketData != null)
                        await ProcessDisableMarketDataRequest(msg.DisableMarketData, webSocket);
                    else if (msg.AccountMetrics != null)
                        await ProcessAccountMetricsequest(msg.AccountMetrics, webSocket);
                }
                catch (Exception ex)
                {
                    break; // disconnected most likely
                }
            }

            if (webSocket.State != WebSocketState.Closed &&
                webSocket.State != WebSocketState.Aborted)
            {
                try
                {
                    await webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure,
                        "Socket closed",
                        CancellationToken.None);
                }
                catch
                {
                    // this may throw on shutdown and can be ignored
                }
            }
        }
        private async Task ProcessDisableMarketDataRequest(DisableMarketDataRequest request, WebSocket webSocket)
        {
            var result = new DisableMarketDataResponse()
            {
                RequestId = request.RequestId,
                TraderId = request.TraderId,
                MarketId = request.MarketId,
                Reason = "success",
                Success = true
            };

            ServerMsg msg = new ServerMsg()
            {
                DisableMarketData = result
            };

            var buffer = msg.ToByteArray();
            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, true, CancellationToken.None);
        }

        private async Task ProcessDisableAccountRequest(DisableAccountRequest request, WebSocket webSocket)
        {
            var result = new DisableAccountResponse()
            {
                RequestId = request.RequestId,
                AccountId = request.AccountId,
                Reason = "success",
                Success = true
            };

            ServerMsg msg = new ServerMsg()
            {
                DisableAccount = result
            };

            var buffer = msg.ToByteArray();
            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, true, CancellationToken.None);
        }

        private async Task ProcessActivateNewMarketDataRequest(ActivateNewMarketDataRequest request, WebSocket webSocket)
        {
            var result = new ActivateNewMarketDataResponse()
            {
                RequestId = request.RequestId,
                TraderId = request.TraderId,
                AgreementUrl = request.AgreementUrl,
                AmpAccountId = request.AmpAccountId,
                CustomerEmail = request.CustomerEmail,
                CustomerName = request.CustomerName,
                MarketId = request.MarketId,
                PersistentMode = request.PersistentMode,
                Reason = "success",
                Success = true
            };

            ServerMsg msg = new ServerMsg()
            {
                ActivateNewMarketData = result
            };

            var buffer = msg.ToByteArray();
            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, true, CancellationToken.None);
        }

        private async Task ProcessNewTradingAccountResetRequest(NewTradingAccountResetRequest request, WebSocket webSocket)
        {
            var result = new NewTradingAccountResetResponse
            {
                RequestId = request.RequestId,
                TraderId = request.TraderId,
                NewAccountId = Convert.ToUInt32(newUserId++),
                NewAccountName = request.NewAccountName,
                RiskAccountCloneId = request.RiskAccountCloneId,
                StartingBalance = request.StartingBalance,
                Reason = "success",
                Success = true
            };

            ServerMsg msg = new ServerMsg()
            {
                NewTradingAccount = result
            };

            var buffer = msg.ToByteArray();
            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, true, CancellationToken.None);
        }

        private async Task ProcessResetPasswordRequest(ResetTraderPasswordRequest request, WebSocket webSocket)
        {
            var result = new ResetTraderPasswordResponse
            {
                RequestId = request.RequestId,
                TraderId = request.TraderId,
                CustomerId = request.CustomerId,
                Success = true
            };

            ServerMsg msg = new ServerMsg()
            {
                ResetPassword = result
            };

            var buffer = msg.ToByteArray();
            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, true, CancellationToken.None);
        }

        private async Task ProcessPongRequest(Pong pong, WebSocket webSocket)
        {
            Console.WriteLine("ProcessPongRequest : ");

            var ping = new Ping
            {
                PingUtcTime = DateTime.Now.ToFileTimeUtc(),
                Token = pong.Token
            };

            ServerMsg msg = new ServerMsg()
            {
                Ping = ping
            };

            var buffer = msg.ToByteArray();

            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, true, CancellationToken.None);
        }

        private async Task ProcessPingRequest(Ping ping, WebSocket webSocket)
        {
            Console.WriteLine("ProcessTraderMetricsRequest : ");

            var pong = new Pong
            {
                PongUtcTime = DateTime.Now.ToFileTimeUtc(),
                Token = ping.Token
            };

            ServerMsg msg = new ServerMsg()
            {
                Pong = pong
            };

            var buffer = msg.ToByteArray();

            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, true, CancellationToken.None);
        }

        private async Task ProcessCqgStatusRequest(CheckCQGStatusRequest req, WebSocket webSocket)
        {
            Console.WriteLine("ProcessCqgStatusRequest : " + req.RequestId);

            var result = new CheckCQGStatusResponse()
            {
                RequestId = req.RequestId,
                CqgConnectionStatus = 1
            };

            ServerMsg msg = new ServerMsg()
            {
                CqgStatus = result
            };

            var buffer = msg.ToByteArray();

            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, true, CancellationToken.None);
        }

        private async Task ProcessTraderMetricsRequest(GetTraderMetricsRequest req, WebSocket webSocket)
        {
            Console.WriteLine("ProcessTraderMetricsRequest : " + req.RequestId);

            GetTraderMetricsResponse res = new GetTraderMetricsResponse();
            res.TraderId = req.TraderId;
            res.RequestId = req.RequestId;
            res.BulkMode = req.BulkMode;

            var m = new AccountMetrics
            {
                Account = "1439599",
                Active = true,
                NetLiqValue = 51012.5,
                SodBalance = 50862.5,
                CurrentDrawdown = 0,
                DrawdownLimit = 2500,
                CurrentDailyPl = 200,
                DailyLossLimit = 1250,
                DrawdownType = "EOD",
                OpenContracts = 4,
                Trader = "G656155",
            };

            
            res.Metrics.Add(m);
            res.Metrics.Add(new AccountMetrics
            {
                Account = "1502723",
                Active = false,
                NetLiqValue = 26400.30,
                SodBalance = 23400.30,
                CurrentDrawdown = 23400.30,
                DrawdownLimit = 23400.30,
                CurrentDailyPl = 23400.30,
                DailyLossLimit = 23400.30,
                DrawdownType = "EOD",
                OpenContracts = 2,
                Trader = "G657789",
            });


           

            ServerMsg msg = new ServerMsg()
            {
                TraderMetrics = res
            };

            var buffer = msg.ToByteArray();

            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, true, CancellationToken.None);

        }

        private async Task ProcessMetaDataRequest(MetaDataRequest metaData, System.Net.WebSockets.WebSocket webSocket)
        {
            Console.WriteLine(metaData.RequestId);

            var result = new MetaDataResponse();
            result.RequestId = metaData.RequestId;
            result.Countries.Add("India");
            result.Countries.Add("USA");
            result.Countries.Add("Canada");
            result.UsStates.Add("New York");
            result.UsStates.Add("Ohio");
            result.UsStates.Add("Missouri");
            result.UsStates.Add("Detroit");
            result.CaStates.Add("Alberta");
            result.CaStates.Add("Manitoba");
            result.CaStates.Add("Nunavut");
            result.CaStates.Add("British Columbia");
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "MetaTrader 5"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "Sierra Chart"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "TT Platform"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "TradingView"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "Jigsaw Trading"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "CQG QTrader"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "CTS"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "R | Trader Pro"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "MotiveWave"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "ADL (Algo Design Lab)"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "CQGM � Mobile"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "VolFix"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "Qcaid Trading Platform"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "Zlantrader"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "R | Trader"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "CQG Trader"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "CQG API"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "AMP WebTrader"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "TT API"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "TT FIX Adapter"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "TigerTrade"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "MultiCharts 12 w/Easy"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "VeloxPro � BookMap"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "ATAS (Advanced Time And Sales) � Order Flow"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "Trading"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "Trade Navigator"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "Investor/RT"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "AgenaTrader"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "iBroker"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "QScalp"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "BlueWater, InsideEdge"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "C2 (Collective 2)"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "eSignal"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "Hidden Force Flux"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "QST � Quick Screen Trading"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "SmartQuant"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "R | API+"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "CQG Spreader"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "CQG Integrated Client"
            });
            result.TradingPlatforms.Add(new CQGEntitlement
            {
                Name = "Barchart Trader"
            });


            result.MarketData.Add(new CQGEntitlement
            {
                Id = "123",
                Name = "EUREX Ultra Non Pro",
                Price = 123.56,
            });

            result.MarketData.Add(new CQGEntitlement
            {
                Id = "51062",
                Name = "CME Non Pro Level 2 Bundled",
                Price = 123.56,
            });
            result.MarketData.Add(new CQGEntitlement
            {
                Id = "123",
                Name = "CME NP L1 Bundled",
                Price = 123.56,
            });

            ServerMsg msg = new ServerMsg()
            {
                MetaData = result
            };

            var buffer = msg.ToByteArray();
            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, true, CancellationToken.None);

        }

        static int newCustomerId = 1000;
        static int newUserId = 500;
        private async Task ProcessNewUserRequest(NewUserRequest newUserRequest, System.Net.WebSockets.WebSocket webSocket)
        {
            Console.WriteLine("NewRequest : " + newUserRequest.RequestId);
            CQGTrader newTrader = new CQGTrader()
            {
                Id = newUserId++.ToString(),
                Name = newUserRequest.FirstName[0].ToString() + newUserRequest.LastName[0].ToString() + randomGen.RandomNumber(100000, 999999).ToString()
            };
            CQGAccount newAccount = new CQGAccount()
            {
                Id = newUserId++.ToString(),
                Name = "st" + randomGen.RandomNumber(100000, 999999).ToString()
            };

            var result = new NewUserResponse
            {
                RequestId = newUserRequest.RequestId,
                Success = true,
                Reason = "Success",
                NewTrader = newTrader,
                NewAccount = newAccount,
                NewCustomerId = newCustomerId++.ToString()
            };

            ServerMsg msg = new ServerMsg()
            {
                NewUser = result
            };
            var buffer = msg.ToByteArray();
            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, true, CancellationToken.None);
        }

        public async Task ProcessLiveMarketDataNoDocsRequest(ActivateLiveMarketDataNoDocsRequest request, System.Net.WebSockets.WebSocket webSocket)
        {
            var result = new ActivateLiveMarketDataNoDocsResponse
            {
                RequestId = request.RequestId,
                ExpirationDate = DateTimeOffset.Now.AddDays(30).ToUnixTimeSeconds(),
                MarketId = request.MarketId,
                TraderId = request.TraderId,
                Success = true
            };

            ServerMsg msg = new ServerMsg()
            {
                ActivateLiveMarketDataNoDocs = result
            };

            var buffer = msg.ToByteArray();
            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, true, CancellationToken.None);

        }

        private async Task ProcessTradingPlatform(ActivateTradingPlatformRequest tradingPlatformRequest, System.Net.WebSockets.WebSocket webSocket)
        {
            var result = new ActivateTradingPlatformResponse
            {
                RequestId = tradingPlatformRequest.RequestId,
                Success = true,
                TraderId = "6"
            };

            ServerMsg msg = new ServerMsg()
            {
                ActivateTradingPlatform = result
            };

            var buffer = msg.ToByteArray();
            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, true, CancellationToken.None);

        }

        private async Task ProcessVerifyEmailRequest(VerifyEmailRequest verifyEmailRequest, System.Net.WebSockets.WebSocket webSocket)
        {
            var result = new VerifyEmailResponse();
            result.RequestId = verifyEmailRequest.RequestId;


            bool isEmail = Regex.IsMatch(verifyEmailRequest.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
            result.IsValid = isEmail;

            if (verifyEmailRequest.Email == "charles1@ninestack.com")
                result.IsValid = false;

            ServerMsg msg = new ServerMsg()
            {
                VerifyEmail = result
            };

            var buffer = msg.ToByteArray();
            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, true, CancellationToken.None);

        }

        private async Task ProcessHasLogon(Logon logon, System.Net.WebSockets.WebSocket webSocket)
        {
            Console.WriteLine(logon.UserName);

            var result = new LogonResult()
            {
                TextMessage = "Logged in"
            };

            ServerMsg msg = new ServerMsg()
            {
                LogonResult = result
            };

            var buffer = msg.ToByteArray();
            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, true, CancellationToken.None);

        }

        private async Task ProcessEnableCQGAccountRequest(EnableCQGAccountRequest req, WebSocket webSocket)
        {
            Console.WriteLine("ProcessEnableCQGAccountRequest : " + req.RequestId);

            EnableCQGAccountResponse res = new EnableCQGAccountResponse();
            res.TraderId = req.TraderId;
            res.RequestId = req.RequestId;
            res.AccountId = req.AccountId;
            res.Success = true;
            res.Reason = string.Empty;

            ServerMsg msg = new ServerMsg()
            {
                EnableCqgAccount = res
            };

            var buffer = msg.ToByteArray();

            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, true, CancellationToken.None);

        }

        static ulong Mt5AccountLogin = 1000;
        private async Task ProcessNewMt5Request(NewSetupMT5Request req, WebSocket webSocket)
        {
            Console.WriteLine("ProcessNewMt5Request : " + req.RequestId);

            NewSetupMT5Response res = new NewSetupMT5Response()
            {
                RequestId = req.RequestId,
                FirstName = req.FirstName,
                LastName = req.LastName,
                Country = req.Country,
                Email = req.Email,
                RiskGroup = req.RiskGroup,
                SendEmail = false,
                Success = true,
                Reason = string.Empty,
                Mt5AccountLogin = Mt5AccountLogin++,
                Mt5AccountPassword = "Test@123"
            };

            ServerMsg msg = new ServerMsg()
            {
                NewMt5 = res
            };

            var buffer = msg.ToByteArray();

            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, true, CancellationToken.None);

        }

        private async Task ProcessResetUserMt5Request(ResetUserMT5Request req, WebSocket webSocket)
        {
            Console.WriteLine("ProcessResetUserMt5Request : " + req.RequestId);

            ResetUserMT5Response res = new ResetUserMT5Response();
            res.RequestId = req.RequestId;
            res.OldMt5AccountLogin = req.OldMt5AccountLogin;
            res.NewMt5AccountLogin = 1001;
            res.NewMt5AccountPassword = "ACC001";
            res.Success = true;
            res.Reason = string.Empty;

            //res.Success = false;
            //res.Reason = "reset user failed";

            ServerMsg msg = new ServerMsg()
            {
                ResetUserMt5 = res
            };

            var buffer = msg.ToByteArray();

            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, true, CancellationToken.None);

        }

        private async Task ProcessResetPassMt5Request(ResetPasswordMT5Request req, WebSocket webSocket)
        {
            Console.WriteLine("ProcessResetPassMt5Request : " + req.RequestId);

            ResetPasswordMT5Response res = new ResetPasswordMT5Response();
            res.RequestId = req.RequestId;
            res.NewMt5AccountPassword = "Test!123";
            res.Mt5AccountLogin = req.Mt5AccountLogin;
            res.Success = true;
            res.Reason = string.Empty;

            //res.Success = false;
            //res.Reason = "reset password failed";

            ServerMsg msg = new ServerMsg()
            {
                ResetPassMt5 = res
            };

            var buffer = msg.ToByteArray();

            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, true, CancellationToken.None);

        }

        private async Task ProcessChangeBalanceMt5Request(ChangeBalanceMT5Request req, WebSocket webSocket)
        {
            Console.WriteLine("ProcessChangeBalanceMt5Request : " + req.RequestId);

            ChangeBalanceMT5Response res = new ChangeBalanceMT5Response();
            res.RequestId = req.RequestId;
            res.NewBalance = req.NewBalance;
            res.Mt5AccountLogin = req.Mt5AccountLogin;
            res.Success = true;
            res.Reason = string.Empty;

            //res.Success = false;
            //res.Reason = "reset user failed";

            ServerMsg msg = new ServerMsg()
            {
                ChangeBalanceMt5 = res
            };

            var buffer = msg.ToByteArray();

            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, true, CancellationToken.None);

        }

        private async Task ProcessDisableUserMt5Request(DisableUserMT5Request req, WebSocket webSocket)
        {
            Console.WriteLine("ProcessNewMt5Request : " + req.RequestId);

            DisableUserMT5Response res = new DisableUserMT5Response();
            res.RequestId = req.RequestId;
            res.Mt5AccountLogin = req.Mt5AccountLogin;
            res.Success = true;
            res.Reason = string.Empty;

            //res.Success = false;
            //res.Reason = "disable user failed";

            ServerMsg msg = new ServerMsg()
            {
                DisableUserMt5 = res
            };

            var buffer = msg.ToByteArray();

            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, true, CancellationToken.None);

        }

        private async Task ProcessAccountMetricsequest(GetAccountMetricsRequest request, WebSocket webSocket)
        {
            var res = new GetAccountMetricsResponse()
            {
                RequestId = request.RequestId,
                AccountId = request.AccountId,
                BulkMode = request.BulkMode
               
            };


            var m = new AccountMetrics
            {
                Account = "1439599",
                Active = true,
                NetLiqValue = 51012.5,
                SodBalance = 50862.5,
                CurrentDrawdown = 0,
                DrawdownLimit = 2500,
                CurrentDailyPl = 200,
                DailyLossLimit = 1250,
                DrawdownType = "EOD",
                OpenContracts = 4,
                Trader = "G675520",
            };


            res.Metrics.Add(m);
            res.Metrics.Add(new AccountMetrics
            {
                Account = "1503117",
                Active = false,
                NetLiqValue = 26400.30,
                SodBalance = 23400.30,
                CurrentDrawdown = 23400.30,
                DrawdownLimit = 23400.30,
                CurrentDailyPl = 23400.30,
                DailyLossLimit = 23400.30,
                DrawdownType = "EOD",
                OpenContracts = 2,
                Trader = "G657789",
            });




            ServerMsg msg = new ServerMsg()
            {
                AccountMetrics = res
                
            };

            var buffer = msg.ToByteArray();

            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Binary, true, CancellationToken.None);

        }
    }
}
